<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="en_US">
<defaultcodec></defaultcodec>
<context>
    <name>AddDialog</name>
    <message>
        <location filename="../src/gui/AddDialog.ui" line="25"/>
        <source>Adding New Word</source>
        <translation>添加新词</translation>
    </message>
    <message>
        <location filename="../src/gui/AddDialog.ui" line="92"/>
        <source>&amp;OK</source>
        <translation>确认(&amp;O)</translation>
    </message>
    <message>
        <location filename="../src/gui/AddDialog.h" line="44"/>
        <source>Get string</source>
        <translation>获取字符串</translation>
    </message>
    <message>
        <location filename="../src/gui/AddDialog.h" line="45"/>
        <source>Input: </source>
        <translation>输入:</translation>
    </message>
</context>
<context>
    <name>BaseScannerWidget</name>
    <message>
        <location filename="../src/gui/BaseScannerWidget.cpp" line="62"/>
        <source>Modify Information</source>
        <translation>修改字典</translation>
    </message>
    <message>
        <location filename="../src/gui/BaseScannerWidget.cpp" line="59"/>
        <source>The dictionary has been modified!</source>
        <translation>字典已经被成功修改！</translation>
    </message>
    <message>
        <location filename="../src/gui/BaseScannerWidget.cpp" line="63"/>
        <source>The dictionary Can Not be modified!</source>
        <translation>字典不能被修改！</translation>
    </message>
    <message>
        <location filename="../src/gui/BaseScannerWidget.cpp" line="33"/>
        <source>Getting New Word</source>
        <translation>正在获取新词</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <location filename="../src/gui/MainWidget.ui" line="13"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/MainWidget.ui" line="25"/>
        <source>&amp;New</source>
        <translation>新建(&amp;N)</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWidget.ui" line="69"/>
        <source>about:blank</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/MainWidget.ui" line="83"/>
        <source>&amp;Delete</source>
        <translation>移除(&amp;D)</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWidget.ui" line="96"/>
        <source>&amp;Recite</source>
        <translation>背诵(&amp;R)</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWidget.ui" line="109"/>
        <source>&amp;Test</source>
        <translation>测试(&amp;T)</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWidget.ui" line="122"/>
        <source>Str&amp;ess</source>
        <translation>难点回顾(&amp;E)</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWidget.ui" line="135"/>
        <source>Stat&amp;us</source>
        <translation>进度信息(&amp;U)</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWidget.ui" line="164"/>
        <source>Sett&amp;ing</source>
        <translation>设置(&amp;I)</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWidget.ui" line="177"/>
        <source>&amp;Quit</source>
        <translation>退出(&amp;Q)</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/gui/MainWindow.cpp" line="96"/>
        <source>Load Error!</source>
        <translation>加载错误！</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWindow.cpp" line="97"/>
        <source>Can&apos;t load the environment variable&#xef;&#xbc;&#x81;</source>
        <translation>无法读取环境变量！</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWindow.cpp" line="114"/>
        <source>Error file name!</source>
        <translation>错误的文件名，无法读取！</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWindow.cpp" line="142"/>
        <source>Can&apos;t creat a task</source>
        <translation>无法创建任务</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWindow.cpp" line="121"/>
        <source>Error file!</source>
        <translation>文件错误！</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWindow.cpp" line="122"/>
        <source> can&apos;t be opened!</source>
        <translation>不能打开！</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWindow.cpp" line="142"/>
        <source>Task information</source>
        <translation>任务信息</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWindow.cpp" line="140"/>
        <source>Creat a task SUCCESS!</source>
        <translation>创建新任务成功！</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWindow.cpp" line="151"/>
        <source>Are you sure you want to remove this task? 
</source>
        <translation>您确定您要移出这个任务吗？</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWindow.cpp" line="152"/>
        <source>Task Name:  </source>
        <translation>任务名:</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWindow.cpp" line="153"/>
        <source>Removing Task ... ...</source>
        <translation>正在移出任务</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWindow.cpp" line="290"/>
        <source>Current:</source>
        <translation>当前词汇:</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWindow.cpp" line="292"/>
        <source>Speed:</source>
        <translation>记忆速度:</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWindow.cpp" line="294"/>
        <source> words/day</source>
        <translation>个/天</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWindow.cpp" line="295"/>
        <source>Done:</source>
        <translation>已掌握:</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWindow.cpp" line="297"/>
        <source>Status Information</source>
        <translation>学习进度</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWindow.cpp" line="301"/>
        <source>The foremost task has been created for </source>
        <translation>当前任务中最早的任务被建立在 </translation>
    </message>
    <message>
        <location filename="../src/gui/MainWindow.cpp" line="302"/>
        <source> days.</source>
        <translation> 天以前。</translation>
    </message>
    <message>
        <location filename="../src/gui/MainWindow.cpp" line="304"/>
        <source>There&apos;s no new tasks!</source>
        <translation>当前没有新任务！</translation>
    </message>
</context>
<context>
    <name>ModifyDialog</name>
    <message>
        <location filename="../src/gui/ModifyDialog.ui" line="25"/>
        <source>Modifying the word</source>
        <translation>正在修改字典</translation>
    </message>
    <message>
        <location filename="../src/gui/ModifyDialog.ui" line="55"/>
        <source>The Special Soundmark Table</source>
        <translation>特殊音标符号对照表</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../src/gui/ModifyDialog.ui" line="92"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:20pt; color:#0000ff;&quot;&gt;θ_ɑ_ʌ_ә_є_æ_ɔ_ʃ_ð_ŋ_ʒ&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:20pt; color:#0000ff;&quot;&gt;0_1_2_3_4_5_6_7_8_9_=&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/ModifyDialog.ui" line="104"/>
        <source>Word:</source>
        <translation>单词:</translation>
    </message>
    <message>
        <location filename="../src/gui/ModifyDialog.ui" line="120"/>
        <source>Soundmark:</source>
        <translation>音标:</translation>
    </message>
    <message>
        <location filename="../src/gui/ModifyDialog.ui" line="136"/>
        <source>Translation:</source>
        <translation>翻译:</translation>
    </message>
    <message>
        <location filename="../src/gui/ModifyDialog.ui" line="186"/>
        <source>&amp;OK</source>
        <translation>确定(&amp;O)</translation>
    </message>
    <message>
        <location filename="../src/gui/ModifyDialog.ui" line="199"/>
        <source>&amp;Cancel</source>
        <translation>取消(&amp;C)</translation>
    </message>
</context>
<context>
    <name>RReciterWidget</name>
    <message>
        <location filename="../src/gui/RReciterWidget.cpp" line="14"/>
        <source>Reverse Reciting</source>
        <translation>正在背诵</translation>
    </message>
    <message>
        <location filename="../src/gui/RReciterWidget.cpp" line="29"/>
        <source>Amount: </source>
        <translation>总数:</translation>
    </message>
    <message>
        <location filename="../src/gui/RReciterWidget.cpp" line="31"/>
        <source>Used Time: </source>
        <translation>使用时间:</translation>
    </message>
    <message>
        <location filename="../src/gui/RReciterWidget.cpp" line="32"/>
        <source> min</source>
        <translation>分钟</translation>
    </message>
    <message>
        <location filename="../src/gui/RReciterWidget.cpp" line="33"/>
        <source>Recite Rate: </source>
        <translation>速度:</translation>
    </message>
    <message>
        <location filename="../src/gui/RReciterWidget.cpp" line="36"/>
        <source> word/min</source>
        <translation>单词/分</translation>
    </message>
    <message>
        <location filename="../src/gui/RReciterWidget.cpp" line="37"/>
        <source>There are </source>
        <translation>提示：其中有</translation>
    </message>
    <message>
        <location filename="../src/gui/RReciterWidget.cpp" line="38"/>
        <source> words you have remembered before.</source>
        <translation>个词是您已经记住的。</translation>
    </message>
</context>
<context>
    <name>RScannerWidget</name>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="13"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="64"/>
        <source>about:blank</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="90"/>
        <source>WidgetType</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="108"/>
        <source>Amount:</source>
        <translation>总数:</translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="215"/>
        <source>00</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="154"/>
        <source>Raw:</source>
        <translation>生词:</translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="200"/>
        <source>R_Times:</source>
        <translation>剩余次数:</translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="250"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:15pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:9pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="279"/>
        <source>&amp;Add</source>
        <translation>添加(&amp;A)</translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="310"/>
        <source>&amp;Delete</source>
        <translation>删除(&amp;D)</translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="335"/>
        <source>&amp;Modify</source>
        <translation>修改(&amp;M)</translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="360"/>
        <source>Press &quot;Up&quot; derectely</source>
        <translation>可直接按方向键“UP”</translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="363"/>
        <source>&amp;Prono</source>
        <translation>发音(&amp;P)</translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="388"/>
        <source>&amp;Stop</source>
        <translation>中止(&amp;S)</translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="411"/>
        <source>Word:</source>
        <translation>单词:</translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="459"/>
        <source>&lt;font color=red&gt;Have You Remember It?&lt;/font&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="497"/>
        <source>Press &quot;Down&quot; derectely</source>
        <translation>可直接按方向键“DOWN”</translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="500"/>
        <source>Answer</source>
        <translation>提示词:</translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="525"/>
        <source>Press &quot;Left&quot; derectely</source>
        <translation>可直接按方向键“DOWN”</translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="528"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="553"/>
        <source>Press &quot;Right&quot; derectely</source>
        <translation>可直接按方向键“RIGHT”</translation>
    </message>
    <message>
        <location filename="../src/gui/RScannerWidget.ui" line="556"/>
        <source>No</source>
        <translation>否</translation>
    </message>
</context>
<context>
    <name>RStressWidget</name>
    <message>
        <location filename="../src/gui/RStressWidget.cpp" line="21"/>
        <source>Review Stress Words</source>
        <translation>难点单词回顾</translation>
    </message>
    <message>
        <location filename="../src/gui/RStressWidget.cpp" line="31"/>
        <source>Amount: </source>
        <translation>总数:</translation>
    </message>
    <message>
        <location filename="../src/gui/RStressWidget.cpp" line="32"/>
        <source>Used Time: </source>
        <translation>使用时间:</translation>
    </message>
    <message>
        <location filename="../src/gui/RStressWidget.cpp" line="33"/>
        <source>Score: </source>
        <translation>得分:</translation>
    </message>
    <message>
        <location filename="../src/gui/RStressWidget.cpp" line="44"/>
        <source> min</source>
        <translation>分钟</translation>
    </message>
</context>
<context>
    <name>RTesterWidget</name>
    <message>
        <location filename="../src/gui/RTesterWidget.cpp" line="16"/>
        <source>Testing</source>
        <translation>正在测试</translation>
    </message>
    <message>
        <location filename="../src/gui/RTesterWidget.cpp" line="31"/>
        <source>Amount: </source>
        <translation>总数:</translation>
    </message>
    <message>
        <location filename="../src/gui/RTesterWidget.cpp" line="33"/>
        <source>Used Time: </source>
        <translation>使用时间:</translation>
    </message>
    <message>
        <location filename="../src/gui/RTesterWidget.cpp" line="34"/>
        <source> min</source>
        <translation>分钟</translation>
    </message>
    <message>
        <location filename="../src/gui/RTesterWidget.cpp" line="35"/>
        <source>Score: </source>
        <translation>得分:</translation>
    </message>
    <message>
        <location filename="../src/gui/RTesterWidget.cpp" line="42"/>
        <source>You passed it!</source>
        <translation>您通过了本次测试！</translation>
    </message>
    <message>
        <location filename="../src/gui/RTesterWidget.cpp" line="44"/>
        <source>You haven&apos;t passed it!</source>
        <translation>您没有通过本次测试，要加油哦！</translation>
    </message>
    <message>
        <location filename="../src/gui/RTesterWidget.cpp" line="46"/>
        <source>You have complished this task!</source>
        <translation>您已经基本掌握了此任务中的单词！</translation>
    </message>
</context>
<context>
    <name>ReciterWidget</name>
    <message>
        <location filename="../src/gui/ReciterWidget.cpp" line="13"/>
        <source>Reciting</source>
        <translation>正在背诵</translation>
    </message>
    <message>
        <location filename="../src/gui/ReciterWidget.cpp" line="27"/>
        <source>Amount: </source>
        <translation>总数:</translation>
    </message>
    <message>
        <location filename="../src/gui/ReciterWidget.cpp" line="29"/>
        <source>Used Time: </source>
        <translation>使用时间:</translation>
    </message>
    <message>
        <location filename="../src/gui/ReciterWidget.cpp" line="30"/>
        <source> min</source>
        <translation>分钟</translation>
    </message>
    <message>
        <location filename="../src/gui/ReciterWidget.cpp" line="31"/>
        <source>Recite Rate: </source>
        <translation>速度:</translation>
    </message>
    <message>
        <location filename="../src/gui/ReciterWidget.cpp" line="35"/>
        <source> word/min</source>
        <translation>单词/分钟</translation>
    </message>
    <message>
        <location filename="../src/gui/ReciterWidget.cpp" line="36"/>
        <source>There are </source>
        <translation>提示：其中有</translation>
    </message>
    <message>
        <location filename="../src/gui/ReciterWidget.cpp" line="37"/>
        <source> words you have remembered before.</source>
        <translation>个词是您已经记住的。</translation>
    </message>
</context>
<context>
    <name>ResultWidget</name>
    <message>
        <location filename="../src/gui/ResultWidget.ui" line="16"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/ResultWidget.ui" line="97"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:15pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Result Information&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/ResultWidget.ui" line="227"/>
        <source>&amp;Confirm</source>
        <translation>确认(&amp;C)</translation>
    </message>
</context>
<context>
    <name>ScanModeDialog</name>
    <message>
        <location filename="../src/gui/ScanModeDialog.ui" line="25"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/ScanModeDialog.ui" line="52"/>
        <source>Scan Mode</source>
        <translation>模式选择</translation>
    </message>
    <message>
        <location filename="../src/gui/ScanModeDialog.ui" line="72"/>
        <source>&amp;Normal</source>
        <translation>正常(&amp;N)</translation>
    </message>
    <message>
        <location filename="../src/gui/ScanModeDialog.ui" line="93"/>
        <source>&amp;Reverse</source>
        <translation>反向(&amp;R)</translation>
    </message>
    <message>
        <location filename="../src/gui/ScanModeDialog.ui" line="121"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../src/gui/ScanModeDialog.ui" line="139"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>ScannerWidget</name>
    <message>
        <location filename="../src/gui/ScannerWidget.ui" line="13"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/ScannerWidget.ui" line="30"/>
        <source>WidgetType</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/ScannerWidget.ui" line="92"/>
        <source>about:blank</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/ScannerWidget.ui" line="110"/>
        <source>Amount:</source>
        <translation>总数:</translation>
    </message>
    <message>
        <location filename="../src/gui/ScannerWidget.ui" line="217"/>
        <source>00</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/ScannerWidget.ui" line="156"/>
        <source>Raw:</source>
        <translation>生词:</translation>
    </message>
    <message>
        <location filename="../src/gui/ScannerWidget.ui" line="202"/>
        <source>R_Times:</source>
        <translation>剩余次数:</translation>
    </message>
    <message>
        <location filename="../src/gui/ScannerWidget.ui" line="250"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:15pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:9pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/ScannerWidget.ui" line="284"/>
        <source>&amp;Add</source>
        <translation>添加(&amp;A)</translation>
    </message>
    <message>
        <location filename="../src/gui/ScannerWidget.ui" line="328"/>
        <source>&amp;Delete</source>
        <translation>移除(&amp;D)</translation>
    </message>
    <message>
        <location filename="../src/gui/ScannerWidget.ui" line="366"/>
        <source>&amp;Modify</source>
        <translation>修改(&amp;M)</translation>
    </message>
    <message>
        <location filename="../src/gui/ScannerWidget.ui" line="404"/>
        <source>&amp;Hint</source>
        <translation>提示(&amp;H)</translation>
    </message>
    <message>
        <location filename="../src/gui/ScannerWidget.ui" line="442"/>
        <source>&amp;Prono</source>
        <translation>发音(&amp;P)</translation>
    </message>
    <message>
        <location filename="../src/gui/ScannerWidget.ui" line="480"/>
        <source>&amp;Stop</source>
        <translation>中止(&amp;S)</translation>
    </message>
    <message>
        <location filename="../src/gui/ScannerWidget.ui" line="497"/>
        <source>Answer:</source>
        <translation>答案:</translation>
    </message>
    <message>
        <location filename="../src/gui/ScannerWidget.ui" line="537"/>
        <source>Input:</source>
        <translation>输入:</translation>
    </message>
    <message>
        <location filename="../src/gui/ScannerWidget.cpp" line="102"/>
        <source> can&apos;t be found!
</source>
        <translation> 无法找到！</translation>
    </message>
</context>
<context>
    <name>SettingWidget</name>
    <message>
        <location filename="../src/gui/SettingWidget.cpp" line="73"/>
        <source>Setting Directory</source>
        <translation>环境设置</translation>
    </message>
    <message>
        <location filename="../src/gui/SettingWidget.ui" line="13"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/SettingWidget.ui" line="79"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:16pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:18pt; color:#182ad3;&quot;&gt;Settings&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/SettingWidget.ui" line="120"/>
        <source>Global Path:</source>
        <translation>全局路径:</translation>
    </message>
    <message>
        <location filename="../src/gui/SettingWidget.ui" line="251"/>
        <source>other ...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/SettingWidget.ui" line="176"/>
        <source>Home Path:</source>
        <translation>用户目录:</translation>
    </message>
    <message>
        <location filename="../src/gui/SettingWidget.ui" line="225"/>
        <source>Pronounce Path:</source>
        <translation>语音库路径:</translation>
    </message>
    <message>
        <location filename="../src/gui/SettingWidget.ui" line="280"/>
        <source>Font:</source>
        <translation>字体</translation>
    </message>
    <message>
        <location filename="../src/gui/SettingWidget.ui" line="324"/>
        <source>Bold</source>
        <translation>粗体</translation>
    </message>
    <message>
        <location filename="../src/gui/SettingWidget.ui" line="336"/>
        <source>Italic</source>
        <translation>斜体</translation>
    </message>
    <message>
        <location filename="../src/gui/SettingWidget.ui" line="360"/>
        <source>Size:</source>
        <translation>字号</translation>
    </message>
    <message>
        <location filename="../src/gui/SettingWidget.ui" line="391"/>
        <source>Sound Effect:</source>
        <translation>音效</translation>
    </message>
    <message>
        <location filename="../src/gui/SettingWidget.ui" line="424"/>
        <source>Allow the sound effect</source>
        <translation>允许音效</translation>
    </message>
    <message>
        <location filename="../src/gui/SettingWidget.ui" line="477"/>
        <source>&amp;Ok</source>
        <translation>确定(&amp;O)</translation>
    </message>
    <message>
        <location filename="../src/gui/SettingWidget.ui" line="490"/>
        <source>&amp;Cancel</source>
        <translation>取消（&amp;C)</translation>
    </message>
</context>
<context>
    <name>StressWidget</name>
    <message>
        <location filename="../src/gui/StressWidget.cpp" line="22"/>
        <source>Review Stress Words</source>
        <translation>难点回顾</translation>
    </message>
    <message>
        <location filename="../src/gui/StressWidget.cpp" line="31"/>
        <source>Amount: </source>
        <translation>总数:</translation>
    </message>
    <message>
        <location filename="../src/gui/StressWidget.cpp" line="32"/>
        <source>Used Time: </source>
        <translation>使用时间:</translation>
    </message>
    <message>
        <location filename="../src/gui/StressWidget.cpp" line="33"/>
        <source>Score: </source>
        <translation>得分:</translation>
    </message>
    <message>
        <location filename="../src/gui/StressWidget.cpp" line="44"/>
        <source> min</source>
        <translation>分钟</translation>
    </message>
</context>
<context>
    <name>TaskModel</name>
    <message>
        <location filename="../src/gui/TaskModel.cpp" line="29"/>
        <source>TaskID</source>
        <translation>任务ID</translation>
    </message>
    <message>
        <location filename="../src/gui/TaskModel.cpp" line="30"/>
        <source>Task Name</source>
        <translation>任务名</translation>
    </message>
    <message>
        <location filename="../src/gui/TaskModel.cpp" line="30"/>
        <source>Step</source>
        <translation>复习阶段</translation>
    </message>
    <message>
        <location filename="../src/gui/TaskModel.cpp" line="32"/>
        <source>Review Time</source>
        <translation>复习时间</translation>
    </message>
</context>
<context>
    <name>TesterWidget</name>
    <message>
        <location filename="../src/gui/TesterWidget.cpp" line="16"/>
        <source>Testing</source>
        <translation>正在测试</translation>
    </message>
    <message>
        <location filename="../src/gui/TesterWidget.cpp" line="31"/>
        <source>Amount: </source>
        <translation>总数:</translation>
    </message>
    <message>
        <location filename="../src/gui/TesterWidget.cpp" line="33"/>
        <source>Used Time: </source>
        <translation>使用时间:</translation>
    </message>
    <message>
        <location filename="../src/gui/TesterWidget.cpp" line="34"/>
        <source> min</source>
        <translation>分钟</translation>
    </message>
    <message>
        <location filename="../src/gui/TesterWidget.cpp" line="35"/>
        <source>Score: </source>
        <translation>得分:</translation>
    </message>
    <message>
        <location filename="../src/gui/TesterWidget.cpp" line="44"/>
        <source>You passed it!</source>
        <translation>您通过了本次测试！</translation>
    </message>
    <message>
        <location filename="../src/gui/TesterWidget.cpp" line="46"/>
        <source>You haven&apos;t passed it!</source>
        <translation>您未能通过本次测试，要加油哦！</translation>
    </message>
    <message>
        <location filename="../src/gui/TesterWidget.cpp" line="48"/>
        <source>You have complished this task!</source>
        <translation>您已经基本掌握了本组词汇！</translation>
    </message>
</context>
</TS>
