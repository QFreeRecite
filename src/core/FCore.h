//This header is used to include all the headers of core lib.

#ifndef FREERECITE_FCORE_H_
#define FREERECITE_FCORE_H_

#include "Debug.h"
#include "Dict.h"

#include "Manager.h"
#include "Task.h"
#include "Exporter.h"
#include "ConfigHolder.h"

#include "Scanner.h"
#include "Reciter.h"
#include "Tester.h"
#include "Stress.h"

#endif //FREERECITE_FCORE_H_
