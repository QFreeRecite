#include "ConfigHolder.h"
#include "Stress.h"

namespace freeRecite {

bool Stress::load() {
  if( loadWords(configHolder.keyFile().c_str(),true) ) {
    wordList = new WordList(words.size(),configHolder.s_list());
    std::vector<std::string>::const_iterator itr = words.begin();
    while(itr != words.end()) {
      errWords.insert(*itr);
      ++itr;
    }
    return true;
  }else {
    return false;
  }
}

void Stress::test(bool result) {
  if(result) {
    if(wordList->status() == 0) {
      errWords.erase(getWord());
      ++score;
    }
    wordList->pass();
  } else {
    wordList->lose();
  }
}

bool Stress::remove(const std::string &word) {
  errWords.erase(word);
  Scanner::remove(word);
}

} //End of namespace freeRecite
