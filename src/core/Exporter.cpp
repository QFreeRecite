#include <vector>
#include <fstream>
#include <algorithm>
#include <ctime>
#include <set>
#include "ConfigHolder.h"
#include "Exporter.h"
#include "WordList.h"
#include "Debug.h"

namespace freeRecite {

Exporter::~Exporter() {
  /* Do Nothing Here! */ 
  D_OUTPUT("~Stress()\n");
} 


bool Exporter::load(time_t initID) {
  if( loadWords(initID,false) ) {
    wordList = new WordList(words.size(),configHolder.e_list());
    return true;
  }else {
    return false;
  }
}

bool Exporter::load(const char *fileName) {
  if( loadWords(fileName,false) ) {
    wordList = new WordList(words.size(),configHolder.e_list());
    return true;
  }else {
    return false;
  }
}

void Exporter::test(bool result) {
  wordList->pass();
}

} //namespace freeRecite end
