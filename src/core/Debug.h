#ifndef FR_DEBUG_H_
#define FR_DEBUG_H_

#if defined(DEBUG)
#  include <iostream>
#  define D_OUTPUT(a) std::cerr << (a) << std::endl;
#else  //DEBUG
#  define D_OUTPUT(a) //Empty
#endif //DEBUG

#endif //FR_DEBUG_H_
