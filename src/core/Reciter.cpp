#include <vector>
#include <fstream>
#include <algorithm>
#include <ctime>
#include <set>
#include "ConfigHolder.h"
#include "Reciter.h"
#include "WordList.h"
#include "Debug.h"

namespace freeRecite {

Reciter::~Reciter() {
  D_OUTPUT("~Reciter()\n");
}

bool Reciter::load(time_t initID) {
  if( loadWords(initID,false) ) {
    wordList = new WordList(words.size(),configHolder.r_list());
    return true;
  }else {
    return false;
  }
}

void Reciter::test(bool result) {
  if(result) {
    if(wordList->status() == 0)
      ++score;
    wordList->pass();
  } else {
    wordList->lose();
  }
}

} //namespace freeRecite end
