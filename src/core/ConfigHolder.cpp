#include <cstdlib>
#include <limits>
#include <string>
#include "Debug.h"
#include "ConfigHolder.h"

namespace freeRecite {

ConfigHolder::ConfigHolder() {
  //Do Nothing Here!
}

void ConfigHolder::load(const char *rootDir, 
			const char *gDir, const char *soundDir) {
  rootDirectory = rootDir;
  globalDirectory = gDir;
  if(soundDir != 0)
    soundDirectory = soundDir;
#ifdef DEBUG
  std::cerr << "rootDir() " << this->rootDir() << std::endl;
  std::cerr << "tasksDir()" << tasksDir() << std::endl;
  std::cerr << "mgrFile()"  << mgrFile() << std::endl;
  std::cerr << "localDictFile()" << localDictFile() << std::endl;
  std::cerr << "globalDictFile()" << globalDictFile() << std::endl;
  std::cerr << "keyFile()" << keyFile() << std::endl;
  std::cerr << "doneFile()" << doneFile() << std::endl;
#endif //DEBUG
}

void ConfigHolder::setRootDir(const char *dir) {
  rootDirectory = dir;
}

void ConfigHolder::setGlobalDir(const char *dir) {
  globalDirectory = dir;  
}

void ConfigHolder::soundDir(const char *dir) {
  soundDirectory = dir;
}


const std::vector<unsigned> *ConfigHolder::e_list() {
  initPt.clear();
  initPt.push_back(0); //list[0] is just a placeholder.
  return &initPt;
}

const  std::vector<unsigned> *ConfigHolder::r_list() {
  initPt.clear();
  initPt.push_back(0); //list[0] is just a placeholder.
  initPt.push_back(1);
  initPt.push_back(3);
  initPt.push_back(5);
  return &initPt;
}

const std::vector<unsigned> *ConfigHolder::s_list() {
  initPt.clear();
  initPt.push_back(0); //list[0] is just a placeholder.
  initPt.push_back(3);
  initPt.push_back(5);
  initPt.push_back(std::numeric_limits<unsigned>::max());
  return &initPt;
}

const std::vector<unsigned> *ConfigHolder::t_list() {
  initPt.clear();
  initPt.push_back(0); //list[0] is just a placeholder.
  return &initPt;
}


//This is a global variable.
ConfigHolder configHolder;

}//End of namespaec freeRecite.
