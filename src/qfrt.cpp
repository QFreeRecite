#include <QApplication>
#include <QPlastiqueStyle>
#include <QLibrary>
#include <QSettings>
#include <QDir>
#include <QTranslator>
#include <QFileInfo>
#include <QFileInfoList>

#include <Debug.h>
#include <MainWindow.h>
#include <ConfigHolder.h>
#include <fstream>

int main(int argc, char *argv[])
{
  QApplication myapp(argc,argv);
  QApplication::setStyle(new QPlastiqueStyle);
  QSettings *settings = new QSettings("QFreeRecite");
  QTranslator translator;
  translator.load(settings->value("GlobalPath")
		  .toString() + "qfrt_zh_CN");
  myapp.installTranslator(&translator);
  QLibrary libgui, libcore;
  libgui.setFileName("libFreeReciteGui");
  libcore.setFileName("libFreeReciteCore");
  D_OUTPUT(libgui.load()?"true":"false")
  D_OUTPUT(libcore.load()?"true":"false")
  MainWindow mainWindow;
  QFile file( QDir::toNativeSeparators(settings->value("GlobalPath")
 				       .toString() + "qss/style.qss") );
  file.open(QFile::ReadOnly);
  myapp.setStyleSheet(file.readAll());
  settings->deleteLater();
  mainWindow.show();
  return myapp.exec();
}
