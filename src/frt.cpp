#include <string>
#include <cstdlib>
#include <ConfigHolder.h>
#include <Cui.h>

using namespace std;

int main(int argc, char *argv[])
{
  freeRecite::configHolder
    .load(std::string(::getenv("HOME")).append("/.FreeRecite/").c_str(),
	  "/usr/share/FreeRecite/");
  freeRecite::CUI cui;
  cui.run(argc,argv);
  return 0;
}
