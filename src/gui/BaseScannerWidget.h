/**
 * This file is a part of the application QFreeRecite.
 *
 * Copyright (C) 2008 Kermit Mei <kermit.mei@gmail.com>
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation.
 *
 * Written by Kermit Mei <kermit.mei@gmail.com>
 *
 * Many of the ideas implemented here are from the author's experiment.
 * But the dictionary's format coincide with the other word recite software
 * to help the users get more available data. And the review times designed
 * by means of the theory named Forgetting Curve which dicoveried by the
 * German psychologist named Hermann Ebbinghaus(1850–1909).
 *
 **/ 

#ifndef FR_BASESCANNERWIDGET_H
#define FR_BASESCANNERWIDGET_H

#include <QWidget>

namespace freeRecite {
  class Scanner;
}

class ResultWidget;
class AddDialog;
class ListModel;

class BaseScannerWidget : public QWidget
{
  Q_OBJECT
public:
  BaseScannerWidget(QWidget *parent = 0);
  virtual ~BaseScannerWidget();
  ResultWidget *getResult()
  { return resultWidget; }
signals:
  void finished();
  void showResult();
protected slots:
  virtual void displayResult() = 0;
  virtual void pronounce() = 0;
  virtual void showNext() = 0;
  virtual void showAnswer() = 0;
  virtual void setInfo() = 0;
  void add();
  void modify();
  void remove();
protected:
  freeRecite::Scanner *scanner;
  ResultWidget *resultWidget;
  time_t startTime;
  ListModel *listModel;
};

#endif //FR_BASESCANNERWIDGET_H
