#include <QSettings>
#include <QMessageBox>
#include <QScrollBar>
#include <QChar>
#include <QUrl>
#include <QFont>

#include <FCore.h>
#include "ScannerWidget.h"
#include "ListModel.h"
#include "Speaker.h"

using namespace freeRecite;

ScannerWidget::ScannerWidget(QWidget *parent)
  :BaseScannerWidget(parent)
{
  setupUi(this);
  QSettings settings("QFreeRecite");
  listModel = new ListModel(this);
  listView->setModel(listModel);
  listView->setAlternatingRowColors(true);
  QFont font(settings.value("FontFamily").toString(),
	     settings.value("FontSize").toInt(),
	     ( settings.value("FontBold").toBool()
	       ? QFont::Bold : QFont::Normal ),
	     settings.value("FontItalic").toBool() );
  textBrowser->setCurrentFont(font);
  connect(lineEdit,SIGNAL(returnPressed()),
	  this,SLOT(lineReturned()));
  connect(addButton,SIGNAL(clicked()),
	  this,SLOT(add()));
  connect(deleteButton,SIGNAL(clicked()),
	  this,SLOT(remove()));
  connect(modifyButton,SIGNAL(clicked()),
	  this,SLOT(modify()));
  connect(pronounceButton,SIGNAL(clicked()),
	  this,SLOT(pronounce()));
  connect(hintButton,SIGNAL(clicked()),
	  this,SLOT(hint()));
  connect(stopButton,SIGNAL(clicked()),
	  this,SIGNAL(finished()));
}

ScannerWidget::~ScannerWidget() {
  D_OUTPUT("~ScannerWidget()")
}

void ScannerWidget::lineReturned() {
  setInfo();
  if(lineEdit->isFreeze()) {
    showNext();
  }else {
    showAnswer();
  }
}

void ScannerWidget::showNext() {
  QString text;
  if(!scanner->isValid()){
    displayResult();
    return;
  }
  lineEdit->setFreeze(false);
  answerLabel->setText(QString(""));
  if(dictionary.lookUp(scanner->getWord())) {
    text = QString::fromUtf8(dictionary.translation().c_str());
    if(!dictionary.example().empty())
	text += "<br /><br />Examples:<br />" + QString::fromUtf8(dictionary.example().c_str());
  }else {
    text = QString(scanner->getWord().c_str())
      + QString(" can't be found!");
  }
  text.replace(scanner->getWord().c_str(),"~",Qt::CaseInsensitive);
  textBrowser->setText(text);
}

void ScannerWidget::showAnswer() {
  lineEdit->setFreeze(true);
  if(lineEdit->text().toStdString() != scanner->getWord()) {
    Speaker::play("fail");
    answerLabel->setText("<font color=red>" +
			 QString(scanner->getWord().c_str()) +
			 "</font>");
  } else {
    Speaker::play("ok");
    answerLabel->setText("<font color=green>" +
			 QString(scanner->getWord().c_str()) +
			 "</font>");
  }
  QString text;
  if(dictionary.lookUp(scanner->getWord())) { 
    if(!dictionary.phonetics().empty()) {
      text.append(dictionary.word().c_str())
	.append(QString::fromUtf8("<br />["))
	.append(QString::fromUtf8(dictionary.phonetics().c_str()))
	.append(QString::fromUtf8("]"));
      text.append("<br />");
    }

    text.append(QString::fromUtf8(dictionary.translation().c_str()));
    if(!dictionary.example().empty())
	text.append("<br /><br />Examples:<br />")
	    .append(QString::fromUtf8(dictionary.example().c_str()));
  }else {
    text = QString(scanner->getWord().c_str())
      + QString(tr(" can't be found!\n"));
  }
  textBrowser->clearHistory();
  if(lineEdit->text().toStdString() != scanner->getWord())
    textBrowser->setText("<font color=red>" + text + "</font>");
  else
    textBrowser->setText(text);
  unsigned tmp = scanner->size();
  scanner->test(lineEdit->text().toStdString() == scanner->getWord());
  if(tmp != scanner->size()) {
    listModel->addToList( dictionary.word().c_str());
    listView->scrollTo(listModel->index(listModel->rowCount() - 1));
  }
  setInfo();
}

void ScannerWidget::setInfo(){
  amountLabel->setText(QVariant(scanner->capability()).toString());
  rawLabel->setText(QVariant(scanner->size()).toString());
  r_timesLabel->setText(QVariant(scanner->times()).toString());
}

void ScannerWidget::pronounce(){
  Speaker::speak(dictionary.word().c_str());
}

void ScannerWidget::hint() {
  if(!scanner->isValid() || lineEdit->isFreeze())
    return;
  QString hintText("");
  for(unsigned i=0,j=4; i < scanner->getWord().size(); ++i,++j) {
    if(j == 4 || scanner->getWord()[i] == ' ') {
      hintText.append(QChar(scanner->getWord().at(i)));
      if(scanner->getWord()[i] == ' ')
	hintText.append(QChar(scanner->getWord().at(++i)));
      j = 0;
    }
    else
      hintText.append(QChar('-'));
  }
  answerLabel->setText("<font color=blue>" +
		       hintText +
		       "</font>");
}
