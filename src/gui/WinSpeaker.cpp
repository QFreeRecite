#include <windows.h>
#include <cstdio>
#include "WinSpeaker.h"

//By default, mutex should be nonrecursive.
QMutex WinSpeaker::mutex(QMutex::NonRecursive);

WinSpeaker::WinSpeaker(QObject *parent)
  : QThread(parent) {
  /*Do Nothing Here!*/
}

bool WinSpeaker::speak(const std::string &wordFile) {
  if(!mutex.tryLock()){
    return false;
  }else {
    word = wordFile;
    start();
    return true;
  }
}

void WinSpeaker::run() {
  PlaySound(word.c_str(), 0, SND_FILENAME|SND_ASYNC);
  mutex.unlock();
}
