/**
 * This file is a part of the application QFreeRecite.
 *
 * Copyright (C) 2008 Kermit Mei <kermit.mei@gmail.com>
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation.
 *
 * Written by Kermit Mei <kermit.mei@gmail.com>
 *
 * Many of the ideas implemented here are from the author's experiment.
 * But the dictionary's format coincide with the other word recite software
 * to help the users get more available data. And the review times designed
 * by means of the theory named Forgetting Curve which dicoveried by the
 * German psychologist named Hermann Ebbinghaus(1850–1909).
 *
 **/ 

#ifndef FR_SCANNERWIDGET_H
#define FR_SCANNERWIDGET_H

#include <QWidget>
#include "BaseScannerWidget.h"
#include "ui_ScannerWidget.h"

namespace freeRecite {
  class Scanner;
}

class ScannerWidget : public BaseScannerWidget,
		      public Ui::ScannerWidget
{
  Q_OBJECT
public:
  ScannerWidget(QWidget *parent = 0);
  virtual ~ScannerWidget();
protected slots:
  virtual void pronounce();
  virtual void showNext();
  virtual void showAnswer();
  virtual void setInfo();
  void hint();
  void lineReturned();
};

#endif //FR_SCANNERWIDGET_H
