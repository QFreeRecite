#include <QProcess>
#include <QString>
#include <QStringList>
#include <QSettings>
#include <QFileInfo>
#include <Debug.h>
#include <ConfigHolder.h>
#include "Speaker.h"

#ifdef WIN32
#include <QApplication>
#include "WinSpeaker.h"
WinSpeaker winSpeaker;
#endif //WIN32

using freeRecite::configHolder;

namespace Speaker {

QSettings settings("QFreeRecite");

QString getWordPath(const QString &word) {
    QString filePath = settings.value("PronouncePath").toString()
	+ word.at(0) + "/" + word + ".wav";

    if(QFile::exists(filePath)) {
	return filePath;
    }

    filePath = settings.value("GlobalPath").toString() + "patchWRPTTS/"
	+ word.at(0) + "/" + word + ".wav";

    if(QFile::exists(filePath)) {
	return filePath;
    }

    filePath = settings.value("HomePath").toString()
	+ "patchWRPTTS/" + word + ".wav";

    if(QFile::exists(filePath)) {
	return filePath;
    }
    
#ifdef UNIX
    static QString oldWord = "";
    if(oldWord != word) {
	oldWord = word;
	if(!word.contains(' ') && !word.contains('-'))
	    QProcess::startDetached(settings.value("GlobalPath").toString() + "gws",
				    QStringList(word));
    }
#endif //UNIX
    return "";
}

void speak(const QString &word) {
    QString fileName = getWordPath(word.toLower());
    if(fileName.isEmpty())
	return ;
    QFileInfo finfo(fileName);
#ifdef WIN32
    winSpeaker.speak(finfo.absoluteFilePath().toStdString());
#else 
    QProcess::startDetached("aplay",
			    QStringList(finfo.absoluteFilePath()));
#endif
}

void play(const QString &sound) {
  if(!settings.value("SoundEffectOpen").toBool())
    return;
  std::string fileName = configHolder.musicDir() 
    + sound.toStdString() + ".wav";
#ifdef WIN32
  //If thread can't play it, then paly in a new process!
  winSpeaker.speak(fileName);
#else 
  QProcess::startDetached("aplay",
			  QStringList(fileName.c_str()));
#endif
}

} //namespace Speaker
