#include "ModifyDialog.h"

QString ModifyDialog::str("");

ModifyDialog::ModifyDialog(QWidget *parent)
  : QDialog(parent)
{
  setupUi(this);
}

ModifyDialog::~ModifyDialog()
{

}

QString ModifyDialog::getDicItmeString(const std::string &defaultWord,
				       const std::string &defaultPhon,
				       const std::string &defaultTran,
				       const std::string &defaultExam,
				       QWidget *parent) {
  str = "";
  ModifyDialog *dialog = new ModifyDialog(parent);
  dialog->setModal(true);
  dialog->wordLineEdit->setText(defaultWord.c_str());
  dialog->soundmarkLineEdit->setText(defaultPhon.c_str());
  dialog->translationTextEdit->setText(QString::fromUtf8(defaultTran.c_str()));
  dialog->exampleTextEdit->setText(QString::fromUtf8(defaultExam.c_str()));
  connect(dialog->okButton,SIGNAL(clicked()),
	  dialog,SLOT(Ok()));
  connect(dialog->cancelButton,SIGNAL(clicked()),
	  dialog,SLOT(Cancel()));
  dialog->exec();
  return str;
}

void ModifyDialog::Cancel() {
  ModifyDialog::str = "";
  deleteLater();
}

void ModifyDialog::Ok() {
  QString usage = translationTextEdit->toPlainText();
  usage.replace("\n","<br />");

  QString example = exampleTextEdit->toPlainText();
  example.replace("\n","<br />");

  ModifyDialog::str = "[W]" + wordLineEdit->text() +
      "[T]" + soundmarkLineEdit->text() +
      "[M]" + usage +
      "[E]" + example;
  deleteLater();
}
