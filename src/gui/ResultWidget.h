/**
 * This file is a part of the application QFreeRecite.
 *
 * Copyright (C) 2008 Kermit Mei <kermit.mei@gmail.com>
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation.
 *
 * Written by Kermit Mei <kermit.mei@gmail.com>
 *
 * Many of the ideas implemented here are from the author's experiment.
 * But the dictionary's format coincide with the other word recite software
 * to help the users get more available data. And the review times designed
 * by means of the theory named Forgetting Curve which dicoveried by the
 * German psychologist named Hermann Ebbinghaus(1850–1909).
 *
 **/ 

#ifndef FR_RESULTWIDGET_H
#define FR_RESULTWIDGET_H

#include <QWidget>
#include <QString>
#include "ui_ResultWidget.h"

class ResultWidget : public QWidget,
		     public Ui::ResultWidget
{
  Q_OBJECT
public:
  ResultWidget(QWidget *parent = 0);
  ~ResultWidget();
  void setHeader(const QString &str);
  void setTailer(const QString &str);
  void setInfo( const QString &str11,const QString &str12,
	        const QString &str21,const QString &str22,
	        const QString &str31,const QString &str32 );
signals:
  void confirmed();
};

#endif //FR_RESULTWIDGET_H
