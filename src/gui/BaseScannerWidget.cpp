#include <QMessageBox>
#include <QScrollBar>
#include <QChar>
#include <QUrl>


#include <FCore.h>
#include "BaseScannerWidget.h"
#include "ModifyDialog.h"
#include "ListModel.h"
#include "ResultWidget.h"
#include "AddDialog.h"
#include "Speaker.h"

using namespace freeRecite;

BaseScannerWidget::BaseScannerWidget(QWidget *parent)
  :QWidget(parent),scanner(0),resultWidget(0),
   startTime(0),listModel(0)
{
  //Do nothing here!
}

BaseScannerWidget::~BaseScannerWidget() {
  if(scanner != 0)
    delete scanner;
  if(qobject_cast<QObject *>(resultWidget) != 0)
    resultWidget->deleteLater();
  D_OUTPUT("~BaseScannerWidget()")
}

void BaseScannerWidget::add() { 
  QString word = AddDialog::getString(this,tr("Getting New Word"));
  if(!word.isEmpty()){
    Speaker::play("ok");    
    scanner->add(word.toStdString());
  }else
    Speaker::play("fail");
  setInfo();
}

void BaseScannerWidget::remove(){
  Speaker::play("remove");
  scanner->remove(scanner->getWord());
  showNext();
  setInfo();
}

void BaseScannerWidget::modify(){
  QString word;
  if(dictionary.lookUp(scanner->getWord()))  {
    word = ModifyDialog::getDicItmeString(dictionary.word(),
					  dictionary.rawPhonetics(),
					  dictionary.translation(),
					  dictionary.example(),
					  this);
  } else { //Can't find the word in dictionary.
    word = ModifyDialog::getDicItmeString(scanner->getWord(),
					  "",
					  "",
					  "",
					  this);
  }
  if(dictionary.modify(word.toUtf8().constData())) {
    Speaker::play("ok");
    showNext();
    QMessageBox::information(this,tr("Modify Information"),
			  tr("The dictionary has been modified!"));
  } else {
    Speaker::play("fail");
    QMessageBox::warning(this,tr("Modify Information"),
			  tr("The dictionary Can Not be modified!"));
  }
}
