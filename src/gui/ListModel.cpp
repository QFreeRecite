#include <QtGui>
#include <QMessageBox>
#include <set>
#include <fstream>
#include <iostream>
#include <Manager.h>
#include <Dict.h>
#include <ConfigHolder.h>
#include <Exporter.h>
#include <ctime>
#include "ListModel.h"

using namespace freeRecite;

ListModel::ListModel(QObject *parent)
  : QAbstractListModel(parent)
{
  //Do Nothing Here!
}

ListModel::~ListModel() {
#ifdef DEBUG
  qDebug("~ListModel()");
#endif //DEBUG
}

void ListModel::addToList(const QString &word) {
  if(!words.contains(word)){
    words.append(word);
    emit layoutChanged();
  }
}

int ListModel::rowCount(const QModelIndex& parent) const
{
  return static_cast<int>(words.size());
}

QVariant ListModel::data(const QModelIndex &index, int role) const {
  if(!index.isValid() || role != Qt::DisplayRole)
    return QVariant();
  else  
    return QVariant(words[index.row()]);
}
