#include "PushButton.h"
#include "Speaker.h"

PushButton::PushButton(QWidget *parent)
  : QPushButton(parent)
{
  //Do Nothing;
  connect(this,SIGNAL(pressed()),this,SLOT(pressSound()));
  connect(this,SIGNAL(released()),this,SLOT(pressSound()));
  setFont(QFont("WenQuanYi",16,12,true));
}

void PushButton::enterEvent(QEvent *event) {
  Speaker::play("buttonactive");
  QPushButton::enterEvent(event);
}

void PushButton::pressSound() const{
  if(objectName() != "pronounceButton")
    Speaker::play("buttondown");
}

void PushButton::releaseSound() const{
  if(objectName() != "pronounceButton")
    Speaker::play("buttonup");
}
