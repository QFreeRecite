#include <QtGui>
#include <QMessageBox>
#include <set>
#include <fstream>
#include <iostream>
#include <Manager.h>
#include <Dict.h>
#include <ConfigHolder.h>
#include <Exporter.h>
#include <ctime>
#include "TaskModel.h"

using namespace freeRecite;

TaskModel::TaskModel(QObject *parent)
  : QAbstractTableModel(parent),showAll(false)
{
  populateModel();
}

TaskModel::~TaskModel() {
  //Do Nothing Here!
}

void TaskModel::populateModel()
{
  header.clear();
  header << tr("TaskID")
	 << tr("Task Name")
	 << tr("Step");
  if(showAll == true)
    header << tr("Review Time");
}

int TaskModel::columnCount(const QModelIndex& parent) const
{
  return header.size();
}

int TaskModel::rowCount(const QModelIndex& parent) const
{
  return static_cast<int>(manager.getActiveTasks().size());
}

QVariant TaskModel::data(const QModelIndex &index, int role) const {
  if (role == Qt::TextAlignmentRole ) {
    return Qt::AlignHCenter;
  }
  if(!index.isValid() || role != Qt::DisplayRole
     || !manager.isValid())
    return QVariant();
  
  time_t taskID = manager.getActiveTasks().at(index.row());
  switch(index.column()) {
  case 0: //TaskID
    return QVariant(static_cast<unsigned>(taskID));
  case 1: //TaskName
    return QVariant(manager.getTaskName(taskID).c_str());
  case 2: //Step
    return QVariant(manager.getTaskStep(taskID));
  case 3:
    time_t nextTime = manager.getNextTime(taskID);
    struct tm * timeinfo;
    char buffer[30];
    timeinfo = localtime(&nextTime);
    strftime(buffer,30,"%Y-%m-%d %H:%M",timeinfo);
    return QVariant(buffer);
  }  //switch() end
  return QVariant();
}

time_t TaskModel::getTaskID(const QModelIndex &index) const {
  unsigned taskNum = manager.getActiveTasks().size();
  if(index.row() >= 0 
     && index.row() < static_cast<int>(taskNum))
    return manager.getActiveTasks().at(index.row());
  else
    return 0;
}

void TaskModel::updateMode() {
  emit layoutChanged();
}

QVariant 
TaskModel::headerData(int section, 
		      Qt::Orientation orientation, int role) const
{
  if(role == Qt::DisplayRole && orientation == Qt::Horizontal)
    return header[section];
  return QAbstractTableModel::headerData(section, orientation, role);
}
