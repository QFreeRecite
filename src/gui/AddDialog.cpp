#include <Debug.h>
#include "AddDialog.h"


QString AddDialog::str("");

AddDialog::AddDialog(QWidget *parent)
  : QDialog(parent)
{
  setupUi(this);
  D_OUTPUT("AddDialog()")
}

AddDialog::~AddDialog()
{
  D_OUTPUT("~AddDialog()")
}

QString AddDialog::getString(QWidget *parent,
			     const QString &titleText,
			     const QString &lableText) {
  str = "";
  AddDialog *dialog = new AddDialog(parent);
  dialog->setWindowTitle(titleText);
  dialog->label->setText(lableText);
  dialog->setModal(true);
  connect(dialog->okButton,SIGNAL(clicked()),
	  dialog,SLOT(returnStr()));
  dialog->exec();
  return str;
}

void AddDialog::returnStr() {
  AddDialog::str = lineEdit->text();
  deleteLater();
}
