#include <Tester.h>
#include <Manager.h>
#include "TesterWidget.h"
#include "ResultWidget.h"

using freeRecite::manager;

TesterWidget::TesterWidget(QWidget *parent)
  : ScannerWidget(parent)
{ /* Do Nothing Here! */ }

void TesterWidget::start(time_t taskID) {
  freeRecite::Tester *tester = new freeRecite::Tester;
  tester->load(taskID);
  scanner = dynamic_cast<freeRecite::Scanner *>(tester);
  topicLabel->setText(tr("Testing"));
  setInfo();
  showNext();
  time(&startTime);
}

void TesterWidget::displayResult() {
  time_t endTime = 0;
  time(&endTime);
  startTime = endTime - startTime;
  startTime /= 60;
  startTime = startTime > 1 ? startTime : 1;

  QString str11,str12,str21,str22,str31,str32;
  unsigned score = scanner->getScore();
  str11 = tr("Amount: ");
  str12 = QVariant(scanner->capability()).toString();
  str21 = tr("Used Time: ");
  str22 = QVariant(static_cast<unsigned>(startTime)).toString() + tr(" min");
  str31 = tr("Score: ");
  str32 = QVariant(score).toString();

  resultWidget = new ResultWidget(this);
  connect(resultWidget,SIGNAL(confirmed()),this,SIGNAL(finished()));

  resultWidget->setInfo(str11,str12,str21,str22,str31,str32);
  int testResult = manager.test(scanner->id(),scanner->getScore());
  if(testResult == 0) {
    resultWidget->setTailer(tr("You passed it!"));
  }else if(testResult == -1){
    resultWidget->setTailer(tr("You haven't passed it!")); 
  }else {  // testResult == 1
    resultWidget->setTailer(tr("You have complished this task!")); 
  }
  manager.refresh();
  emit showResult();
}
