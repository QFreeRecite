#include <QKeyEvent>
#include "LineEdit.h"
#include "Speaker.h"

LineEdit::LineEdit(QWidget *parent)
  : QLineEdit(parent)
{
  //Do Nothing;
}

void LineEdit::keyPressEvent(QKeyEvent *event) {
  if(freeze == false) {
    if( (event->key() >= Qt::Key_A && event->key() <= Qt::Key_Z) ||
	event->key() == Qt::Key_Space )
      Speaker::play("type");
    else if(event->key() == Qt::Key_Backspace)
      Speaker::play("remove");
  }
  QLineEdit::keyPressEvent(event);
}

bool LineEdit::isFreeze() const {
  return freeze;
}

void LineEdit::setFreeze(bool flag) {
  freeze = flag;
  setReadOnly(freeze);
  if(!freeze)
    clear();
}
