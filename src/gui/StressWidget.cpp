#include <Stress.h>
#include <Manager.h>
#include <Debug.h>
#include "StressWidget.h"
#include "ResultWidget.h"

using freeRecite::manager;

StressWidget::StressWidget(QWidget *parent)
  : TesterWidget(parent)
{ 
  D_OUTPUT("StressWidget::StressWidget()")
}

void StressWidget::start() {
  freeRecite::Stress *stress = new freeRecite::Stress;
  if(!stress->load()) {
    //scanner = 0 by default;
    displayResult();
    return;
  }else {
    scanner = dynamic_cast<freeRecite::Scanner *>(stress);
    topicLabel->setText(tr("Review Stress Words"));
    setInfo();
    showNext();
    time(&startTime);
  }
}

void StressWidget::displayResult() {
  QString str11,str12,str21,str22,str31,str32;
  str11 = tr("Amount: "); 
  str21 = tr("Used Time: ");
  str31 = tr("Score: ");
  resultWidget = new ResultWidget(this);
  if(scanner != 0 && scanner->capability() != 0) {
    time_t endTime = 0;
    time(&endTime);
    startTime = endTime - startTime;
    startTime /= 60;
    startTime = startTime > 1 ? startTime : 1;
    str22 = QVariant(static_cast<unsigned>(startTime)).toString() + tr(" min");
    str12 = QVariant(scanner->capability()).toString();
    str22 = QVariant(static_cast<unsigned>(startTime)).toString() + tr(" min");
    str32 = QVariant(scanner->getScore()).toString();
  }else {
    str12 = "0"; 
    str22 = "0 min";
    str32 = "0";
    resultWidget->setTailer("There's no word needs to be reveiwed!");
  }
  resultWidget->setInfo(str11,str12,str21,str22,str31,str32);
  connect(resultWidget,SIGNAL(confirmed()),this,SIGNAL(finished()));
  emit showResult();
}
