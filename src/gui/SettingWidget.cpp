#include <QSettings>
#include <QFileDialog>
#include <QVariant>
#include <FCore.h>

#include "SettingWidget.h"

SettingWidget::SettingWidget(QWidget *parent)
  : QWidget(parent)
{
  setupUi(this);
  QSettings settings("QFreeRecite");
  if(settings.value("SoundEffectOpen").toBool())
    soundRButton->setDown(true);
  else
    soundRButton->setDown(false);
  gPathBox->insertItem(0,settings.value("GlobalPath").toString());
  gPathBox->setCurrentIndex(0);
  hPathBox->insertItem(0,settings.value("HomePath").toString());
  hPathBox->setCurrentIndex(0);
  pPathBox->insertItem(0,settings.value("PronouncePath").toString());
  pPathBox->setCurrentIndex(0);
  fontBox->setCurrentFont(settings.value("FontFamily").toString());
  boldCheckBox->setDown(settings.value("FontBold").toBool());
  italicCheckBox->setDown(settings.value("FontItalic").toBool());
  if(settings.contains("FontSize"))
    fontSizeSpinBox->setValue(settings.value("FontSize").toInt());
  connect(gPathBox,SIGNAL(activated(int)),
	  this,SLOT(setPathBox(int)));
  connect(hPathBox,SIGNAL(activated(int)),
	  this,SLOT(setPathBox(int)));
  connect(pPathBox,SIGNAL(activated(int)),
	  this,SLOT(setPathBox(int)));
  connect(okButton,SIGNAL(clicked()),this,SLOT(okPressed()));
  connect(cancelButton,SIGNAL(clicked()),this,SIGNAL(finished()));
}

SettingWidget::~SettingWidget()
{
  D_OUTPUT("~SettingWidget()")
}

void SettingWidget::okPressed() {
  QSettings settings("QFreeRecite");
  settings.setValue("HomePath",hPathBox->currentText());
  settings.setValue("GlobalPath",gPathBox->currentText());
  settings.setValue("PronouncePath",pPathBox->currentText());
  settings.setValue("SoundEffectOpen",soundRButton->isChecked());
  settings.setValue("FontFamily",fontBox->currentFont().family());
  settings.setValue("FontSize",fontSizeSpinBox->value());
  settings.setValue("FontBold",boldCheckBox->isChecked());
  settings.setValue("FontItalic",italicCheckBox->isChecked());

  freeRecite::configHolder
    .load( settings.value("HomePath").toString().toStdString().c_str(),
	   settings.value("GlobalPath").toString().toStdString().c_str(),
	   settings.value("PronouncePath").toString().toStdString().c_str() );
  freeRecite::manager.load();
  freeRecite::dictionary.load();
  emit finished();
}

void SettingWidget::setPathBox(int index) {
  QComboBox *comboBox = qobject_cast<QComboBox *>(sender());
  if( comboBox == 0 || index != comboBox->count() -1)
    return;
#ifdef WIN32
  QString dir = 
    QDir::toNativeSeparators( 
          QFileDialog::getExistingDirectory(this,tr("Setting Directory")) + "/"
			      );
#else
  QString dir = QFileDialog::getExistingDirectory(this,tr("Setting Directory"));
#endif
  if(!dir.isEmpty())
    comboBox->insertItem(0,dir);
  comboBox->setCurrentIndex(0);
}
