#include <Reciter.h>
#include "ReciterWidget.h"
#include "ResultWidget.h"

ReciterWidget::ReciterWidget(QWidget *parent)
  :ScannerWidget(parent)
{ /* Do Nothing Here! */ }

void ReciterWidget::start(time_t taskID) {
  freeRecite::Reciter *reciter = new freeRecite::Reciter;
  reciter->load(taskID);
  scanner = dynamic_cast<freeRecite::Scanner *>(reciter);
  topicLabel->setText(tr("Reciting"));
  setInfo();
  showNext();
  time(&startTime);
}

void ReciterWidget::displayResult() {
  time_t endTime = 0;
  time(&endTime);
  startTime = endTime - startTime;
  startTime /= 60;
  startTime = startTime > 1 ? startTime : 1;

  QString str11,str12,str21,str22,str31,str32,strTail;
  str11 = tr("Amount: ");
  str12 = QVariant(scanner->capability()).toString();
  str21 = tr("Used Time: ");
  str22 = QVariant(static_cast<unsigned>(startTime)).toString() + tr(" min");
  str31 = tr("Recite Rate: ");
  str32 = 
    QVariant(static_cast<float>(scanner->capability() - 
				scanner->getScore())/startTime).toString()
    + tr(" word/min");
  strTail = tr("There are ") + QVariant(scanner->getScore()).toString() +
    tr(" words you have remembered before.");
  resultWidget = new ResultWidget(this);
  connect(resultWidget,SIGNAL(confirmed()),this,SIGNAL(finished()));
  resultWidget->setInfo(str11,str12,str21,str22,str31,str32);
  resultWidget->setTailer(strTail);
  emit showResult();
}
