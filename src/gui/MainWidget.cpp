#include <QHeaderView>
#include <QModelIndex>
#include <Reciter.h>
#include <fstream>
#include <set>

#include <Debug.h>
#include "MainWidget.h"
#include "TaskModel.h"

MainWidget::MainWidget(QWidget *parent)
  : QWidget(parent),taskModel(0)
{
  setupUi(this);
  setWindowFlags(Qt::FramelessWindowHint);
  taskModel = new TaskModel(this);
  taskTableView->setModel(taskModel);
  taskTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
  taskTableView->setSelectionMode(QAbstractItemView::SingleSelection);
  taskTableView->setShowGrid(false);
  taskTableView->resizeColumnsToContents();
  taskTableView->setAlternatingRowColors(true);
  taskTableView->horizontalHeader()->setStretchLastSection(true);
  if(taskModel->rowCount() > 0)
    taskTableView->setCurrentIndex(taskModel->index(0,0));
}

MainWidget::~MainWidget() {
  D_OUTPUT("~MainWidget")
}

time_t MainWidget::getCurrentTaskID() const {
  QModelIndex currIndex = taskTableView->selectionModel()->currentIndex();
  return taskModel->getTaskID(currIndex);
}

void MainWidget::updateMode() {
  taskModel->updateMode();
}
