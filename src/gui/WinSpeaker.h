#ifndef WINSPEAK_H_
#define WINSPEAK_H_

#include <QThread>
#include <QMutex>
#include <string>

class WinSpeaker : public QThread
{
  Q_OBJECT
public:
  WinSpeaker(QObject *parent = 0);
  virtual ~WinSpeaker()
  { /* Do Nothing Here */ }
  bool speak(const std::string &wordFile);
protected:
  virtual void run();
  std::string word;
  static QMutex mutex;
};

#endif //WINSPEAK_H_
