#include <QFileDialog>
#include <QMessageBox>
#include <QStatusBar>
#include <QSettings>
#include <set>
#include <fstream>
#include <string>
#include <QIcon>
#include <FCore.h>
#include "FGui.h"

using namespace freeRecite;

MainWindow::MainWindow(QWidget *parent)
  :QMainWindow(parent),stackedWidget(0),mainWidget(0)
{
  D_OUTPUT("MainWindow::MainWindow()")
  setFixedSize(770,480);
  setWindowTitle("Free Recite");
  setWindowIcon(QIcon(":/icon/myapp.ico"));
  statusBar()->hide();
  loadEnv();
  if(!manager.load())
    close();
  if(!dictionary.load())
    close();
  stackedWidget = new QStackedWidget(this);
  setCentralWidget(stackedWidget);
  mainWidget = new MainWidget(this);
  stackedWidget->addWidget(mainWidget);

  connect(mainWidget->newButton,SIGNAL(clicked()),
	  this,SLOT(addNewTask()));
  connect(mainWidget->removeButton,SIGNAL(clicked()),
	  this,SLOT(remove()));
  connect(mainWidget->quitButton,SIGNAL(clicked()),
	  this,SLOT(close()));

  connect(mainWidget->reciteButton,SIGNAL(clicked()),
	  this,SLOT(recite()));
  connect(mainWidget->testButton,SIGNAL(clicked()),
	  this,SLOT(test()));
  connect(mainWidget->stressButton,SIGNAL(clicked()),
	  this,SLOT(stress()));


  connect(mainWidget->settingButton,SIGNAL(clicked()),
	  this,SLOT(setEnv()));
  connect(mainWidget->statusButton,SIGNAL(clicked()),
	  this,SLOT(showStatus()));

}

MainWindow::~MainWindow() {
  if(stackedWidget != 0)
    stackedWidget->deleteLater();
  if(mainWidget != 0)
    stackedWidget->deleteLater();
  D_OUTPUT("MainWindow::~MainWindow()")
}

void MainWindow::loadEnv()
{
  QSettings settings("QFreeRecite");
  if(!settings.contains("HomePath")) {
#ifdef WIN32
    settings.setValue("HomePath",
	      QDir::toNativeSeparators(QDir::homePath()
			       + "/FreeRecite/"));
    settings.setValue("GlobalPath",
	      QDir::toNativeSeparators(QDir::rootPath()
			       + "Program Files/FreeRecite/"));
    settings.setValue("PronouncePath",
	      QDir::toNativeSeparators(QDir::rootPath()
			       + "Program Files/WyabdcRealPeopleTTS/"));
#else
    settings.setValue("HomePath",
	      QDir::toNativeSeparators(QDir::homePath()
			       + "/.FreeRecite/"));
    settings.setValue("GlobalPath",
	      QDir::toNativeSeparators("/usr/share/FreeRecite/"));
    settings.setValue("PronouncePath",
	      QDir::toNativeSeparators("/usr/share/WyabdcRealPeopleTTS/"));
#endif
  } //End of if(!settings.contains("HomePath"))
  QDir dir(settings.value("HomePath").toString());
  if(!dir.exists("tasks")) {
    dir.mkpath("tasks");
    QString mgrFile = QDir::toNativeSeparators(dir.absolutePath() 
					       + "/freeRecite.mgr");
    std::ofstream fs(mgrFile.toStdString().c_str());
    if(fs.is_open()){
      fs << 0 << ',' << 0 << std::endl;
    }else {
      QMessageBox::warning(this,
			   tr("Load Error!"),
			   tr("Can't load the environment variable！"));
      close();
    }
  }

  //Load the configure.
  freeRecite::configHolder
    .load( settings.value("HomePath").toString().toStdString().c_str(),
	   settings.value("GlobalPath").toString().toStdString().c_str(),
	   settings.value("PronouncePath").toString().toStdString().c_str() );
}



void MainWindow::addNewTask() {
  QString fileName = QFileDialog::getOpenFileName(this);
  if(fileName.isEmpty()) {
    QMessageBox::warning(this,tr("Error file name!"),tr("Can't creat a task"));
    return;
  }
  QString taskName = AddDialog::getString(this,"Getting Task Name",
					  "Task Name: ");
  std::ifstream ifs(fileName.toStdString().c_str());
  if(!ifs.is_open()) {
    QMessageBox::warning(this,tr("Error file!"),
			 fileName + tr(" can't be opened!"));
    return;
  }

  std::set<std::string> wordSet;
  std::string word,name,cmd;
  while(ifs.good()) {
    std::getline(ifs,word);
    while(word[word.size() - 1] == ' ')
      word.erase(word.size() - 1);
    wordSet.insert(word);
  }
  unsigned taskMaxWords = wordSet.size()>30 ? 20 : 30;
  if( manager.createTask(wordSet,
			 taskName.toStdString().c_str(),
			 taskMaxWords)
     )
    QMessageBox::information(this,tr("Task information"),
			  tr("Creat a task SUCCESS!"));
  else
    QMessageBox::warning(this,tr("Task information"),tr("Can't creat a task"));
  mainWidget->updateMode();
}


void MainWindow::remove() {
  if(!manager.hasTask(mainWidget->getCurrentTaskID()))
    return;
  QString quesInfo = tr("Are you sure you want to remove this task? \n")
    + tr("Task Name:  ")
    + manager.getTaskName(mainWidget->getCurrentTaskID()).c_str();
  if( QMessageBox::question(this,tr("Removing Task ... ..."),
			    quesInfo,
			    QMessageBox::Ok,
			    QMessageBox::Cancel)
      == QMessageBox::Ok)
    manager.removeTask(mainWidget->getCurrentTaskID());
  mainWidget->updateMode();
}

void MainWindow::recite() {
  if(mainWidget->getCurrentTaskID() == 0)
    return;
  Type::ModeType type = ScanModeDialog::getMode(this);
  if(type == Type::None)
    return;
  if(type == Type::Normal) {
    ReciterWidget *reciterWidget = new ReciterWidget(this);
    stackedWidget->addWidget(reciterWidget);
    stackedWidget->setCurrentWidget(reciterWidget);
    connect(reciterWidget,SIGNAL(showResult()),
	    this,SLOT(showResult()));
    connect(reciterWidget,SIGNAL(finished()),
	    this,SLOT(showMainFromScan()));
    reciterWidget->start(mainWidget->getCurrentTaskID());
  }else { //flag.first == Reverse
    RReciterWidget *scannerWidget = new RReciterWidget(this);
    stackedWidget->addWidget(scannerWidget);
    stackedWidget->setCurrentWidget(scannerWidget);
    connect(scannerWidget,SIGNAL(showResult()),
	    this,SLOT(showResult()));
    connect(scannerWidget,SIGNAL(finished()),
	    this,SLOT(showMainFromScan()));
    scannerWidget->start(mainWidget->getCurrentTaskID());
  }
}

void MainWindow::test() {
  if(mainWidget->getCurrentTaskID() == 0)
    return;
  Type::ModeType type = ScanModeDialog::getMode(this);
  if(type == Type::None)
    return;
  if(type == Type::Normal) {
    TesterWidget *scannerWidget = new TesterWidget(this);
    stackedWidget->addWidget(scannerWidget);
    stackedWidget->setCurrentWidget(scannerWidget);
    connect(scannerWidget,SIGNAL(showResult()),
	    this,SLOT(showResult()));
    connect(scannerWidget,SIGNAL(finished()),
	    this,SLOT(showMainFromScan()));
    scannerWidget->start(mainWidget->getCurrentTaskID());
  } else { //flag.first == Reverse
    RTesterWidget *scannerWidget = new RTesterWidget(this);
    stackedWidget->addWidget(scannerWidget);
    stackedWidget->setCurrentWidget(scannerWidget);
    connect(scannerWidget,SIGNAL(showResult()),
	    this,SLOT(showResult()));
    connect(scannerWidget,SIGNAL(finished()),
	    this,SLOT(showMainFromScan()));
    scannerWidget->start(mainWidget->getCurrentTaskID());
  }
}

void MainWindow::stress() {
  Type::ModeType type = ScanModeDialog::getMode(this);
  if(type == Type::None)
    return;
  if(type == Type::Normal) {
    StressWidget *scannerWidget = new StressWidget(this);
    stackedWidget->addWidget(scannerWidget);
    stackedWidget->setCurrentWidget(scannerWidget);
    connect(scannerWidget,SIGNAL(showResult()),
	    this,SLOT(showResult()));
    connect(scannerWidget,SIGNAL(finished()),
	    this,SLOT(showMainFromScan()));
    scannerWidget->start();
  } else { //type == Reverse
    RStressWidget *scannerWidget = new RStressWidget(this);
    stackedWidget->addWidget(scannerWidget);
    stackedWidget->setCurrentWidget(scannerWidget);
    connect(scannerWidget,SIGNAL(showResult()),
	    this,SLOT(showResult()));
    connect(scannerWidget,SIGNAL(finished()),
	    this,SLOT(showMainFromScan()));
    scannerWidget->start();
  }
}

void MainWindow::setEnv() {
  SettingWidget *settingWidget = new SettingWidget(this);
  stackedWidget->addWidget(settingWidget);
  stackedWidget->setCurrentWidget(settingWidget);
  connect(settingWidget,SIGNAL(finished()),
	  this,SLOT(loadEnv()));
  connect(settingWidget,SIGNAL(finished()),
	  this,SLOT(showMainFromSetting()));
}

void MainWindow::showMainFromScan() {
  BaseScannerWidget *scannerWidget
    = qobject_cast<BaseScannerWidget *>(sender());
  stackedWidget->setCurrentWidget(mainWidget);
  scannerWidget->deleteLater();
}

void MainWindow::showMainFromSetting() {
  SettingWidget *settingWidget = qobject_cast<SettingWidget *>(sender());
  stackedWidget->setCurrentWidget(mainWidget);
  settingWidget->deleteLater();
}

void MainWindow::showMainFromStatus() {
  ResultWidget *resultWidget = qobject_cast<ResultWidget *>(sender());
  resultWidget->deleteLater();
  stackedWidget->setCurrentWidget(mainWidget);
}

void MainWindow::showResult() {
  BaseScannerWidget *baseScannerWidget
    = qobject_cast<BaseScannerWidget *>(sender());
  stackedWidget->addWidget(baseScannerWidget->getResult());
  stackedWidget->setCurrentWidget(baseScannerWidget->getResult());
  connect(baseScannerWidget,SIGNAL(finished()),
	  this,SLOT(showMainFromScan()));
  mainWidget->updateMode();
}

void MainWindow::showStatus() {
  ResultWidget *resultWidget = new ResultWidget(this);
  QString str11,str12,str21,str22,str31,str32;
  time_t current;
  time_t earlyTask = manager.getAllTasks().begin()->first;
  time(&current);

  current = (current - earlyTask) / (3600 * 24); //Days
  if(current < 1)
    current = 1;
  str11 = tr("Current:");
  str12 = QVariant(manager.currNumWords()).toString();
  str21 = tr("Speed:");   
  str22 = QVariant(static_cast<int>(manager.currNumWords()/current))
    .toString() + tr(" words/day");
  str31 = tr("Done:");
  str32 = QVariant(manager.doneNumWords()).toString();
  resultWidget->setHeader(tr("Status Information"));
  resultWidget->setInfo(str11,str12,str21,str22,str31,str32);
  if(earlyTask != 0)
    resultWidget->setTailer(tr("The foremost task has been created for ")
			    + QVariant(static_cast<int>(current)).toString()
			    + tr(" days."));
  else
    resultWidget->setTailer(tr("There's no new tasks!"));
  stackedWidget->addWidget(resultWidget);
  stackedWidget->setCurrentWidget(resultWidget);
  connect(resultWidget,SIGNAL(confirmed()),
	  this,SLOT(showMainFromStatus()));
  D_OUTPUT(stackedWidget->count());
}
