#include <QFileDialog>
#include <Debug.h>
#include "ScanModeDialog.h"

using namespace Type;

ModeType ScanModeDialog::type(None);

ScanModeDialog::ScanModeDialog(QWidget *parent)
  : QDialog(parent)
{
  setupUi(this);
  type = None;
  D_OUTPUT("ScanModeDialog()")
}

ScanModeDialog::~ScanModeDialog()
{
  D_OUTPUT("~ScanModeDialog()")
}

Type::ModeType
ScanModeDialog::getMode(QWidget *parent)  {
  type = None;
  ScanModeDialog *dialog = new ScanModeDialog(parent);
  dialog->setModal(true);
  connect(dialog->okButton,SIGNAL(clicked()),
	  dialog,SLOT(ok()));
  connect(dialog->cancelButton,SIGNAL(clicked()),
	  dialog,SLOT(deleteLater()));
  dialog->exec();
  return type;
}

void ScanModeDialog::ok() {
  if(normalButton->isChecked())
    type = Normal;
  else
    type = Reverse;
  deleteLater();
}
