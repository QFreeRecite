#include <QScrollBar>
#include <QSettings>
#include <QMessageBox>
#include <QUrl>
#include <QChar>
#include <QKeyEvent>

#include <FCore.h>
#include "RScannerWidget.h"
#include "ListModel.h"
#include "Speaker.h"

using namespace freeRecite;

RScannerWidget::RScannerWidget(QWidget *parent)
  :BaseScannerWidget(parent)
{
  setupUi(this);
  QSettings settings("QFreeRecite");
  setFocus();
  textBrowser->setFontFamily(settings.value("BrowserFont").toString());
  listModel = new ListModel(this);
  listView->setModel(listModel);
  listView->setAlternatingRowColors(true);
  connect(yesButton,SIGNAL(clicked()),
	  this,SLOT(testYes()));
  connect(noButton,SIGNAL(clicked()),
	  this,SLOT(testNo()));
  connect(showAnswerButton,SIGNAL(clicked()),
	  this,SLOT(showAnswer()));
  connect(addButton,SIGNAL(clicked()),
	  this,SLOT(add()));
  connect(deleteButton,SIGNAL(clicked()),
	  this,SLOT(remove()));
  connect(modifyButton,SIGNAL(clicked()),
	  this,SLOT(modify()));
  connect(pronounceButton,SIGNAL(clicked()),
	  this,SLOT(pronounce()));
  connect(stopButton,SIGNAL(clicked()),
	  this,SIGNAL(finished()));
}

RScannerWidget::~RScannerWidget() {
  D_OUTPUT("~RScannerWidget()")
}

void RScannerWidget::keyPressEvent(QKeyEvent * event) {
  if(event->key() == Qt::Key_Down && showAnswerButton->isVisible()) {
    Speaker::play("buttondown");
    showAnswer();
    Speaker::play("buttonup");
  }else if(event->key() == Qt::Key_Left && yesButton->isVisible()) {
    Speaker::play("buttondown");
    testYes();
    Speaker::play("buttonup");
  }else if(event->key() == Qt::Key_Right && noButton->isVisible()) {
    Speaker::play("buttondown");
    testNo();
    Speaker::play("buttonup");
  }else if(event->key() == Qt::Key_Up) {
    pronounce();
  }else {
    QWidget::keyPressEvent(event);
  }
}

void RScannerWidget::showNext() {
  if(!scanner->isValid()){
    displayResult();
    return;
  }
  textBrowser->clear();
  textBrowser->clearHistory();
  textBrowser->setStyleSheet("background-image: url(:/icon/quote.png)");
  answerLabel->setText(scanner->getWord().c_str());
  yesButton->hide();
  noButton->hide();
  showAnswerButton->show();
}

void RScannerWidget::testYes() {
  unsigned tmp = scanner->size();
  scanner->test(true);
  if(tmp != scanner->size()) {
    listModel->addToList( dictionary.word().c_str());
    listView->scrollTo(listModel->index(listModel->rowCount() - 1));
  }
  showNext();
  setInfo();
}

void RScannerWidget::testNo() {
  unsigned tmp = scanner->size();
  scanner->test(false);
  //Call this to skip the current word in reverse mode.
  scanner->test(true);
  if(tmp != scanner->size()) {
    listModel->addToList( dictionary.word().c_str());
    listView->scrollTo(listModel->index(listModel->rowCount() - 1));
  }
  showNext();
  setInfo();
}

void RScannerWidget::showAnswer() {
  yesButton->show();
  noButton->show();
  showAnswerButton->hide();
  QString text = scanner->getWord().c_str();
  answerLabel->setText(text);
  if(dictionary.lookUp(scanner->getWord())) { 
    if(!dictionary.phonetics().empty()) {
      text.append(QString::fromUtf8("<br />["))
	.append(QString::fromUtf8(dictionary.phonetics().c_str()))
	.append(QString::fromUtf8("]"));
    }
    text.append("<br />");
    text.append(QString::fromUtf8(dictionary.translation().c_str()));
    if(!dictionary.example().empty())
	text += "<br /><br />Examples:<br />" + QString::fromUtf8(dictionary.example().c_str());

  }else { //The word can't be found!
    text = QString(scanner->getWord().c_str()) + QString(" can't be found!\n");
  }
  textBrowser->clearHistory();
  textBrowser->setStyleSheet("");
  textBrowser->setText(text);
}

void RScannerWidget::pronounce(){
  Speaker::speak(scanner->getWord().c_str());
}


void RScannerWidget::setInfo(){
  amountLabel->setText(QVariant(scanner->capability()).toString());
  rawLabel->setText(QVariant(scanner->size()).toString());
  r_timesLabel->setText(QVariant(scanner->times()).toString());
}
