/**
 * This file is a part of the application QFreeRecite.
 *
 * Copyright (C) 2008 Kermit Mei <kermit.mei@gmail.com>
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation.
 *
 * Written by Kermit Mei <kermit.mei@gmail.com>
 *
 * Many of the ideas implemented here are from the author's experiment.
 * But the dictionary's format coincide with the other word recite software
 * to help the users get more available data. And the review times designed
 * by means of the theory named Forgetting Curve which dicoveried by the
 * German psychologist named Hermann Ebbinghaus(1850–1909).
 *
 **/ 

#ifndef FR_ADDDIALOG_H
#define FR_ADDDIALOG_H

#include <QDialog>
#include "ui_AddDialog.h"

class AddDialog : public QDialog,
		  public Ui::AddDialog
{
  Q_OBJECT
public:
  AddDialog(QWidget *parent = 0);
  ~AddDialog();
  static QString getString(QWidget *parent = 0,
			   const QString &titleText = tr("Get string"),
			   const QString &lableText = tr("Input: "));
protected slots:
  void returnStr();
protected:
  static QString str;
};
#endif //FR_ADDDIALOG_H
