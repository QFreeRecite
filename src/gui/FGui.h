#ifndef FREERECITE_GUI_H_
#define FREERECITE_GUI_H_

#include "AddDialog.h"
#include "ModifyDialog.h"
#include "ScanModeDialog.h"
#include "SettingWidget.h"
#include "LineEdit.h"
#include "PushButton.h"
#include "ResultWidget.h"

#include "MainWindow.h"
#include "MainWidget.h"
#include "Speaker.h"
#include "TaskModel.h"
#include "ListModel.h"

#include "BaseScannerWidget.h"
#include "ReciterWidget.h"
#include "RReciterWidget.h"
#include "ScannerWidget.h"
#include "RScannerWidget.h"
#include "TesterWidget.h"
#include "RTesterWidget.h"
#include "StressWidget.h"
#include "RStressWidget.h"

#endif //FREERECITE_GUI_H_
