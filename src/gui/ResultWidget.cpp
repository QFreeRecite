#include "ResultWidget.h"

ResultWidget::ResultWidget(QWidget *parent)
  :QWidget(parent)
{
  setupUi(this);
  connect(confirmButton,SIGNAL(clicked()),this,SIGNAL(confirmed()));
}

ResultWidget::~ResultWidget()
{
#ifdef DEBUG
  qDebug("~ResultWidget\n");
#endif //DEBUG
}

void ResultWidget::setHeader(const QString &str) {
  headerLabel->setText(str);
}

void ResultWidget::setTailer(const QString &str) {
  tailerLabel->setText(str);
}

void ResultWidget::setInfo(const QString &str11,const QString &str12,
		      const QString &str21,const QString &str22,
		      const QString &str31,const QString &str32) {
  label_11->setText(str11);
  label_12->setText(str12);

  label_21->setText(str21);
  label_22->setText(str22);

  label_31->setText(str31);
  label_32->setText(str32);
}
